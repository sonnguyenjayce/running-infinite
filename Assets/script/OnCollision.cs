using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnCollision : MonoBehaviour
{
    public PlayerController m_char;
    private void OnCollisionEnter(Collision other) 
    {
        if(other.transform.tag == "player")
            return;
        m_char.OnCharacterColliderHit(other.collider);    
    }
}
