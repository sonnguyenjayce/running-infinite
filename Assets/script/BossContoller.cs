using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossContoller : MonoBehaviour
{
    public Animator bossAnim;
    public Transform boss;
    public float curDis;
    private void Start()
    {
        bossAnim.Play("Run");
    }
    public void Follow(Vector3 pos, float speed)
    {
        Vector3 position = pos - Vector3.forward * curDis;
        boss.position = Vector3.Lerp(boss.position, position, Time.deltaTime * speed / curDis);
        //dog.position = boss.position;
    }
}
