using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleCollection : MonoBehaviour
{
    public List<float> collectabX;
    public List<float> collectabY;
    public float GetLane()
    {
        if (collectabY == null || collectabY.Count < 1)
       {
            return -1f;
        }
        return collectabY[Random.Range(0, collectabY.Count)];
    }
    public float GetJump()
    {
        if(collectabX == null || collectabX.Count < 1)
        {
            return -1f;
        }
        return collectabX[Random.Range(0, collectabX.Count)];
    }
}
