using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemController : MonoBehaviour
{
    [SerializeField] private List<GameObject> _listHouse;
    [SerializeField] private List<Transform> _posSpawn;
    private void Start()
    {
        int i = Random.Range(0,_listHouse.Count);
        GameObject item = _listHouse[i];
        Instantiate(item, _posSpawn[0]);
    }
}
