using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    private float rotationSpeed = 100f;
    void Update()
    {
        this.transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime);
    }
    public void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            PlayerController.numberCoin += 1;
            Debug.Log("Coin : " + PlayerController.numberCoin);
            Destroy(gameObject);
        }
    }
}
