using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionController : MonoBehaviour
{
    private int _initAmount = 4;
    private float _plotSize = 20f;
    private float _lastZone = 2f;
    public List<GameObject> Home;
    private void Start() 
    {
        for (int i = 0; i < _initAmount; i++)
        {
            SpawnPlot();
        }
    }
    public void SpawnPlot()
    {
        int randomVatcan = Random.Range(0, Home.Count);
        GameObject vatCan = Home[randomVatcan];
        int x = Random.Range(-1 , 1);
        float zPos = _lastZone + _plotSize;
        Instantiate(vatCan, new Vector3(x, 0,zPos), vatCan.transform.rotation);
        _lastZone += _plotSize;
    }
}
