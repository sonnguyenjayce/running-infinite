using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnMannager : MonoBehaviour
{
    MapController mapController;
    //PositionController playerController;
    void Start()
    {
        mapController = GetComponent<MapController>();
        //playerController = GetComponent<PositionController>();
    }
    public void SpawnTriggerEntered()
    {
        mapController.MoveRoad();
        //playerController.SpawnPlot();
    }
}
